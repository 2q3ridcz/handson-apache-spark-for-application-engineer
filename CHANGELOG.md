# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.0.5] - 2023-03-25

「4-4 データ収集フローの構築」まで完了

### Added
- Connection between fluentd and kafka

## [0.0.4] - 2023-03-25
### Changed
- To lighter fluentd container image
- Refactor Readme.md

## [0.0.3] - 2023-02-18

「4-3 データ収集詳細 -Apache Kafka詳細-」まで完了

### Added
- Apache kafka container to devcontainer

## [0.0.2] - 2023-02-18

「4-2 データ収集詳細 -Fluentd詳細-」まで完了

### Added
- Fluentd container to devcontainer

## [0.0.1] - 2023-02-13

「2-6 インタラクティブシェルの起動と実行」まで完了

### Added
- Devcontainer for apache spark

[Unreleased]: https://gitlab.com/2q3ridcz/handson-apache-spark-for-application-engineer/-/compare/v0.0.5...main
[0.0.5]: https://gitlab.com/2q3ridcz/handson-apache-spark-for-application-engineer/-/tree/v0.0.5
[0.0.4]: https://gitlab.com/2q3ridcz/handson-apache-spark-for-application-engineer/-/tree/v0.0.4
[0.0.3]: https://gitlab.com/2q3ridcz/handson-apache-spark-for-application-engineer/-/tree/v0.0.3
[0.0.2]: https://gitlab.com/2q3ridcz/handson-apache-spark-for-application-engineer/-/tree/v0.0.2
[0.0.1]: https://gitlab.com/2q3ridcz/handson-apache-spark-for-application-engineer/-/tree/v0.0.1
