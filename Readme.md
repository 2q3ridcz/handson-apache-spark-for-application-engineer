# handson-apache-spark-for-application-engineer

書籍「[アプリケーションエンジニアのためのApache Spark入門](https://books.rakuten.co.jp/rb/15317215/)」の勉強メモ。

## 本の目次

- 1 データ分析プラットフォームの概要
- 2 Sparkの概要
- 3 サンプルユースケース概要
- 4 Fluentd、Kafkaによるデータ収集
- 5 Spark Streamingによるデータ処理
- 6 外部ストレージへのデータ蓄積
- 7 Spark Streamingによるデータ分析
- 8 Spark SQLによるデータ処理
- 9 Spark MLlibによるデータ分析
- 10 プロダクションに向けたシステムアーキテクチャを考える
- Appendix サンプルデータの作成

## メモ

章立てに沿って、気になったとこにコメント。

### 1 データ分析プラットフォームの概要
### 2 Sparkの概要

(「2 Sparkの概要」での成果物は [v0.0.1] 参照。)

#### 2-5 本書で利用する環境

本は Vagrant 使って構築したり Spark 2.2.0 だったりするが、こちらは Spark のダウンロードページで紹介されていた apache/spark-py で VSCode devcontainer 環境を構築。

> Convenience Docker Container Images  
> [Spark Docker Container images are available from DockerHub](https://hub.docker.com/r/apache/spark-py/tags), these images contain non-ASF software and may be subject to different license terms.
> 
> &mdash; [Downloads | Apache Spark](https://spark.apache.org/downloads.html)

環境変数設定のところから本に合流。  
環境変数 `SPARK_HOME` は設定済みだった。
ただし、パスは本に記載の `/usr/local/spark` ではなく `/opt/spark`。
`SPARK_HOME` 以外の環境変数は未設定だったため、本を参考に設定した。

devcontainer 内で作業する。

```bash
# Bash in Spark container
export PATH=${SPARK_HOME}/bin:${PATH}
export PYTHONPATH=${SPARK_HOME}/python/:${SPARK_HOME}/python/lib/py4j-0.10.9.5-src.zip
```


#### 2-6 インタラクティブシェルの起動と実行

起動は簡単。

```bash
# Bash in Spark container
spark-shell
```

だけど停止コマンドが本になくて困った。 `scala>` のあとに `exit` などを打っても停止できず… 正解は

```scala
// SparkShell in Spark container
System.exit(0)
```

pyspark の起動も簡単。こちらは python 同様 `exit()` で停止できる。

```bash
# Bash in Spark container
pyspark
```

次に、「サンプルプログラムの実行」の WordCount へ。 入力ファイルである Spark の README.md が存在せず困ったが、適当なファイルで代用。  
あと、本は python2 系っぽい。こちらは Python 3.9.2 。この差異にも注意。(python3 は print の括弧が必須、など。)

```python
# Pyspark in Spark container
spark = SparkSession.builder.getOrCreate()
lines = spark.read.text("./Readme.md").rdd.map(lambda x: x[0])
count = lines.flatMap(lambda x: x.split(" ")).map(lambda x: (x, 1)).reduceByKey(lambda x, y: x + y).collect()
for (word, count) in count:
    print(f"{word}: {count}")
exit()
```

### 3 サンプルユースケース概要
### 4 Fluentd、Kafkaによるデータ収集

#### 4-2 データ収集詳細 -Fluentd詳細-

(「4-2 データ収集詳細 -Fluentd詳細-」での変更点は [v0.0.1 -> v0.0.2] 参照。)

##### Fluentd 環境構築

Spark の環境構築に成功して喜んでたけど、次は Fluentd の環境構築…

コンテナが 2 つになるため、 DOCKERFILE を使用した devcontainer から docker-compose.yml を使用した devcontainer へ変更。

本では Port 9999 を使用しているが、今回は fluentd の規定 (?) の 24224 を使用する ([Fluentd document](https://docs.fluentd.org/installation/before-install#optimize-the-network-kernel-parameters))。

##### Fluentd 起動

devcontainer 起動後、 Fluentd を起動する。
(docker-compse.yml に組み込める作業もあるだろうけど、勉強のため手作業。)

Fluentd コンテナに入る。

```PowerShell
# PowerShell on docker host
docker exec -it handson-apache-spark-for-application-engineer_devcontainer-fluentd-1 /bin/sh
```

ここからは Fluentd コンテナの中で作業する。Fluentd の image には、元々用意されている config がある。

```shell
# Shell in Fluentd container
# config 存在確認
ls /fluentd/etc/fluent.conf
# config の内容確認
cat /fluentd/etc/fluent.conf
```

が、中身が本と全然違うので今回は使わない。このプロジェクトで用意した config を使用して fluentd を起動する。(docker-compose.yml の設定で、 `/workspace` はこのプロジェクトのルートと連携してある。)

```shell
# Shell in Fluentd container
# config 存在確認 (存在するはず)
ls /workspace/fluentd/etc/fluent.conf
# config の内容確認 (このプロジェクトで用意した config の内容のはず)
cat /workspace/fluentd/etc/fluent.conf
# config を使用して fluentd を起動
fluentd -c /workspace/fluentd/etc/fluent.conf -v
```

画面に log がたくさん表示される。途中、 config の内容も表示されるので、意図した config が使用されていることを確認すること。最後の方に以下が出力されれば起動成功。

```log
2023-02-18 04:29:34 +0000 [info]: #0 fluent/log.rb:330:info: starting fluentd worker pid=132 ppid=127 worker=0
2023-02-18 04:29:34 +0000 [debug]: #0 [input_http] listening http bind="0.0.0.0" port=24224
2023-02-18 04:29:34 +0000 [info]: #0 fluent/log.rb:330:info: fluentd worker is now running worker=0
```

##### Fluentd 動作確認

コンテナの外から Request を投げ、動作確認する。ここでは、コンテナを起動している Windows10 PC の PowerShell から投げる。

```PowerShell
# PowerShell on docker host
# Request 1 - 画面出力あり、ファイル出力なし
$Uri = "http://localhost:24224/debug.test"
$Body = 'json={"message":"message"}'
$Res = Invoke-WebRequest -Uri $Uri -Method Post -Body $Body
Write-Host -Object (@($Res.StatusCode, $Res.StatusDescription, $Body) -join ": ")

# Request 2 - 画面出力なし、ファイル出力あり
$Uri = "http://localhost:24224/sample.test"
$Body = 'json={"message":"これはsampleメッセージです"}'
$Res = Invoke-WebRequest -Uri $Uri -Method Post -Body $Body
Write-Host -Object (@($Res.StatusCode, $Res.StatusDescription, $Body) -join ": ")

# Request 3 - 画面出力なし、ファイル出力なし
$Uri = "http://localhost:24224/sample.test"
$Body = 'json={"message":"これは連携されないメッセージです"}'
$Res = Invoke-WebRequest -Uri $Uri -Method Post -Body $Body
Write-Host -Object (@($Res.StatusCode, $Res.StatusDescription, $Body) -join ": ")
```

PowerShell の実行結果はこうなるはず。

```log
200: OK: json={"message":"message"}
200: OK: json={"message":"これはsampleメッセージです"}
200: OK: json={"message":"これは連携されないメッセージです"}
```

コンテナの中では、以下のような log が出力されているはず。

```log
2023-02-18 04:29:47.981655100 +0000 debug.test: {"message":"message"}
2023-02-18 04:29:48 +0000 [debug]: #0 [out_file_sample] Created new chunk chunk_id="5f4f1e1512f1162dcae172e7283d5e44" metadata=#<struct Fluent::Plugin::Buffer::Metadata timekey=1676678400, tag=nil, variables=nil, seq=0>
```

コンテナの中での作業に戻る。 Ctrl+C で fluentd を停止する。

```log
2023-02-18 04:31:16 +0000 [info]: fluent/log.rb:330:info: Worker 0 finished with status 0
```

ログファイルのパスを取得する。

```shell
# Shell in Fluentd container
ls -l /fluentd/log/sample
```

```log
total 8
-rw-r--r-- 1 fluent fluent 69 Feb 18 04:29 buffer.b5f4f1e1512f1162dcae172e7283d5e44.log
-rw-r--r-- 1 fluent fluent 79 Feb 18 04:29 buffer.b5f4f1e1512f1162dcae172e7283d5e44.log.meta
```

先程のパスを使って、ログファイルの内容を確認する。

```shell
# Shell in Fluentd container
cat /fluentd/log/sample/buffer.b5f4f1e1512f1162dcae172e7283d5e44.log
```

```log
2023-02-18T04:29:48+00:00       sample.test     {"message":"???sample???????"}
```

ちゃんと入ってそう！ (文字化けしてて不確かだけど)

Fluentd のお試し完了。コンテナから抜ける。

```shell
# Shell in Fluentd container
exit
```

#### 4-3 データ収集詳細 -Apache Kafka詳細-

(「4-3 データ収集詳細 -Apache Kafka詳細-」での変更点は [v0.0.2 -> v0.0.3] 参照。)

次は Kafka…

[このサイト](https://betterdatascience.com/how-to-install-apache-kafka-using-docker-the-easy-way/) を参考に docker-compose.yml へ追加。 Kafka の公式 image はなさそうだが、 wurstmeister さんのが主流っぽい。

Kafka コンテナに入る。

```PowerShell
# PowerShell on docker host
docker exec -it kafka /bin/sh
```

```shell
# Shell in Kafka container
# shell の存在確認
ls /opt/kafka/bin/kafka-topics.sh
# topic の作成
/opt/kafka/bin/kafka-topics.sh --create --zookeeper zookeeper:2181 --replication-factor 1 --partitions 1 --topic sample-topic
# topic の配置確認
/opt/kafka/bin/kafka-topics.sh --list --zookeeper zookeeper:2181
# topic の partition 構成確認
/opt/kafka/bin/kafka-topics.sh --describe --zookeeper zookeeper:2181 --topic sample-topic
```

producer コンソールを起動し、作成した topic へメッセージを送信する。

```shell
# Shell in Kafka container
# メッセージを送信するためのコンソールを起動する
/opt/kafka/bin/kafka-console-producer.sh --broker-list localhost:9092 --topic sample-topic
```

```log
>abc
>def
>ghijklmnop
>qrstuv
>wxyz
>^C
```

送信が終わったら、 Ctrl+C でコンソールを終了する。
次は consumer コンソールを起動し、 producer console から送信したメッセージを受信する。

```shell
# Shell in Kafka container
# メッセージを受信するためのコンソールを起動する
/opt/kafka/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic sample-topic --from-beginning
```

```log
abc
def
ghijklmnop
qrstuv
wxyz
^CProcessed a total of 5 messages
```

ちゃんと送受信できた！

Kafka のお試し完了。コンテナから抜ける。

```shell
# Shell in Kafka container
exit
```

#### 4-4 データ収集フローの構築

(「4-4 データ収集フローの構築」での変更点は [v0.0.4 -> v0.0.5] 参照。)

Fluentd を起動する。

```PowerShell
# PowerShell on docker host
docker exec -it handson-apache-spark-for-application-engineer_devcontainer-fluentd-1 /bin/sh
```

```shell
# Shell in Fluentd container
fluentd -c /workspace/fluentd/etc/fluent.conf -v
```

Kafka を設定する。

```PowerShell
# PowerShell on docker host
docker exec -it kafka /bin/sh
```

```shell
# Shell in Kafka container
# topic の作成
/opt/kafka/bin/kafka-topics.sh --create --zookeeper zookeeper:2181 --replication-factor 1 --partitions 1 --topic sensor-data
# topic の partition 構成確認
/opt/kafka/bin/kafka-topics.sh --describe --zookeeper zookeeper:2181 --topic sensor-data
```

連携確認用の consumer を起動する。

```shell
# Shell in Kafka container
/opt/kafka/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic sensor-data
```

コンテナの外から Request を投げ、動作確認する。ここでは、コンテナを起動している Windows10 PC の PowerShell から投げる。

```PowerShell
# PowerShell on docker host
$Uri = "http://localhost:24224/sensor.data"

# Request 1 - 有効データ
$Body = 'json={"id":1851632, "date":"2016-06-01 01:00", "coord":{"lon":143.19, "lat":42.92}, "main":{"temperature":10.6, "humidity":99, "ph":5.5, "whc":64.6}}'
$Res = Invoke-WebRequest -Uri $Uri -Method Post -Body $Body
Write-Host -Object (@($Res.StatusCode, $Res.StatusDescription, $Body) -join ": ")

# Request 2 - 無効データ
$Body = 'json={"id":1851632, "date":"2016-06-01 01:00", "coord":{"lon":143.19, "lat":42.92}, "main":""}'
$Res = Invoke-WebRequest -Uri $Uri -Method Post -Body $Body
Write-Host -Object (@($Res.StatusCode, $Res.StatusDescription, $Body) -join ": ")
```

PowerShell の実行結果はこうなるはず。

```log
200: OK: json={"id":1851632, "date":"2016-06-01 01:00", "coord":{"lon":143.19, "lat":42.92}, "main":{"temperature":10.6, "humidity":99, "ph":5.5, "whc":64.6}}
200: OK: json={"id":1851632, "date":"2016-06-01 01:00", "coord":{"lon":143.19, "lat":42.92}, "main":""}
```

consumer に、下記のように有効データのみの log が出力されれば成功！

```log
{"id":1851632,"date":"2016-06-01 01:00","coord":{"lon":143.19,"lat":42.92},"main":{"temperature":10.6,"humidity":99,"ph":5.5,"whc":64.6}}
```

蛇足だが備忘。この「4-4 データ収集フローの構築」で docker-compose.yml の Kafka コンテナの設定を下記のように変えている。

```yaml
  kafka:
    ...
    extra_hosts:
      - "host.docker.internal:host-gateway"
    environment:
      KAFKA_ADVERTISED_HOST_NAME: host.docker.internal
```

元々 `KAFKA_ADVERTISED_HOST_NAME: localhost` としていたが、これだと Fluentd コンテナから Kafka コンテナへの連携でエラーとなった (参考：[Send exception occurred. Failed to send messages to <topic> · Issue #292 · fluent/fluent-plugin-kafka · GitHub](https://github.com/fluent/fluent-plugin-kafka/issues/292))。

調べている内にこのエラーは、[Kafka の docker image のサイト](https://hub.docker.com/r/wurstmeister/kafka) に記載の下記前提事項への違反に起因していることが分かった。

> - modify the `KAFKA_ADVERTISED_HOST_NAME` in docker-compose.yml to match your docker host IP (Note: Do not use localhost or 127.0.0.1 as the host ip if you want to run multiple brokers.)

そこで、[Docker コンテナ内からホストのポートにアクセスする方法まとめ | gotohayato.com](https://gotohayato.com/content/561/) を参考に `extra_hosts` を追加し、 `KAFKA_ADVERTISED_HOST_NAME` を変更した。

### 5 Spark Streamingによるデータ処理
### 6 外部ストレージへのデータ蓄積
### 7 Spark Streamingによるデータ分析
### 8 Spark SQLによるデータ処理
### 9 Spark MLlibによるデータ分析
### 10 プロダクションに向けたシステムアーキテクチャを考える
### Appendix サンプルデータの作成

[v0.0.4 -> v0.0.5]: https://gitlab.com/2q3ridcz/handson-apache-spark-for-application-engineer/-/compare/v0.0.4...v0.0.5
[v0.0.2 -> v0.0.3]: https://gitlab.com/2q3ridcz/handson-apache-spark-for-application-engineer/-/compare/v0.0.2...v0.0.3
[v0.0.1 -> v0.0.2]: https://gitlab.com/2q3ridcz/handson-apache-spark-for-application-engineer/-/compare/v0.0.1...v0.0.2
[v0.0.1]: https://gitlab.com/2q3ridcz/handson-apache-spark-for-application-engineer/-/tree/v0.0.1
